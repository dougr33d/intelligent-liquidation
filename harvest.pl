#!/usr/bin/perl
use warnings;
use strict;
use Data::Dumper;
use List::MoreUtils qw(uniq);

my @FNS_IN = ();

my $LTG_TAX_DFLT = 0.20;
my $LTG_TAX      = $LTG_TAX_DFLT;
my $STG_TAX_DFLT = 0.35;
my $STG_TAX      = $STG_TAX_DFLT;
my $TARGET_GAINS = -3000;

##############

for (my $i=0; $i<=$#ARGV; $i+=1) {
    if ($ARGV[$i] =~ /^-h|--help$/) {
        usage();
    } elsif ($ARGV[$i] =~ /^-s|--short-term-tax/) {
        $STG_TAX = $ARGV[++$i];
    } elsif ($ARGV[$i] =~ /^-l|--long-term-tax/) {
        $LTG_TAX = $ARGV[++$i];
    } elsif ($ARGV[$i] =~ /^-t|--target-loss/) {
        $TARGET_GAINS = $ARGV[++$i];
    } elsif ($ARGV[$i] =~ /^-/) {
        usage();
    } else {
        push(@FNS_IN, $ARGV[$i]);
    }
}

my $TARGET_LOSS = -1 * $TARGET_GAINS;

if ($STG_TAX < 0.0 or $STG_TAX > 1.0) {
    usage("Error: your short-term gains rate should be between 0 and 1 (and probably much closer to 0 than 1).");
}

if (int(@FNS_IN) == 0) {
    usage("Error: No files specified.");
}

##############

sub usage {
    my ($msg) = @_;

    if(defined($msg)) {
        print("> $msg\n\n");
    }

    print <<EOM;
    Usage: $0 [options] <in-file.txt [in-file2.txt [...]]> 

Options:
    -h,--help                                   this
    -s,--short-term-tax  SHORT_TAX_RATE         your current short-term capital gains rate (default: $STG_TAX_DFLT)
    -l,--long-term-tax   LONG_TAX_RATE          your current long-term capital gains rate (default: $LTG_TAX_DFLT)
    -t,--target-gains    TARGET_GAINS           target gains (default: -3000)
EOM
    exit(-1);
}

##############

sub fmt_num {
    my ($n) = @_;
    return sprintf("%.2f", $n);
}

##############

sub fn_to_dict {
    my ($fn) = @_;

    my $fh_in = undef;
    open($fh_in, "<$fn") or usage("Error: could not open file '$fn' for reading.");

    my @recs = ();
    foreach my $line (<$fh_in>) {
        chomp $line;
        next if ($line =~ /^\s*$/);
        $line =~ s/\$//g;
        $line =~ s/,//g;
        $line =~ s/—/-/g;
        $line =~ s/\s-\s/ 0.00 /g;
        $line =~ s/– ([0-9])/-$1/g;
        #print("$line\n");

        my @field_contents = split(/\s+/, $line);
        my @field_headers  = ("date", "quantity", "cps", "cost", "value", "stgain", "ltgain", "totgain");

        my $n = 0;
        my %rec = ("fn" => $fn);
        foreach my $fhdr (@field_headers) {
            $rec{$fhdr} = $field_contents[$n];
            $n++;
        }
        $rec{"ltg_tax"} = $rec{"ltgain"} * $LTG_TAX;
        $rec{"stg_tax"} = $rec{"stgain"} * $STG_TAX;
        $rec{"tot_tax"} = $rec{"ltg_tax"} + $rec{"stg_tax"};
        $rec{"tpv"}     = $rec{"tot_tax"} / $rec{"value"};

        $rec{"gpv"} = $rec{"totgain"} / $rec{"value"};

        if ($rec{"stgain"} == 0.0) {
            $rec{"lts"} = 1;
        } else {
            $rec{"lts"} = 0;
        }
        push(@recs, \%rec);
    }

    return \@recs;
}

my @recs = ();
foreach my $fn (@FNS_IN) {
    @recs = (@recs, @{ fn_to_dict($fn) });
}

## extract gainers
my @recs_gainers   = grep { $_->{"totgain"} >= 0.0 } @recs;
@recs_gainers = sort { $a->{"tpv"} <=> $b->{"tpv"} } @recs_gainers;

## extract losers
my @recs_losers    = grep { $_->{"totgain"} < 0.0 } @recs;
@recs_losers = sort { $b->{"totgain"} <=> $a->{"totgain"} } @recs_losers;

## Sell sequence
my @sell_order   = (@recs_losers, @recs_gainers);
my $cum_amount     = 0.00;
my $cum_lts_gains  = 0.00;
my $cum_sts_gains  = 0.00;
my $cum_taxes_owed = 0.00;
my $cum_num_shares = 0.00;

printf("%32s          %10s %8s %8s             %8s %8s %7s        %12s (%12s)    %12s %12s\n", 
    "",     
    "Date", "Qty", "QtySum",
    "Value", "Gain", "Tax/V",
    "ValSum", "TaxSum",
    "LTG Sum", "STG Sum"
);

use List::Util qw/reduce/;

sub calc_total_gains {
    my ($recsp) = @_;
    my @recs = @{ $recsp };
    my $gains = reduce { $a + $b } map { $_->{"totgain"} } @recs;
    return $gains;
}

my $total_losses = calc_total_gains(\@recs_losers);
my $total_gains  = calc_total_gains(\@recs_gainers);

sub print_rec {
    my ($recp, $cum_num_shares, $cum_amount, $cum_taxes_owed, $cum_lts_gains, $cum_sts_gains) = @_;
    printf("%32s          %10s %8s %8s             %8s %8s %+0.3f         %12s (%12s)    %12s %12s\n", 
        $recp->{"fn"},
        $recp->{"date"}, fmt_num($recp->{"quantity"}), fmt_num($cum_num_shares),
        fmt_num($recp->{"value"}), fmt_num($recp->{"totgain"}), $recp->{"tpv"},
        fmt_num($cum_amount),    fmt_num($cum_taxes_owed),
        fmt_num($cum_lts_gains), fmt_num($cum_sts_gains),
    );
}

### Cases to consider:
# Goal #1: sell $3k in losses
# Goal #2: sell as many gains as possible, so long as they're offset by losses.

### Goal #1: sell $3k in losses
my $remaining_losses = $TARGET_LOSS;
while ($remaining_losses > 0 && int(@recs_losers)>0) {
    my $rec = shift @recs_losers;
    
    $cum_amount     += $rec->{"value"}; 
    $cum_lts_gains  += $rec->{"ltgain"}; 
    $cum_sts_gains  += $rec->{"stgain"}; 
    $cum_taxes_owed += $rec->{"tot_tax"};
    $cum_num_shares += $rec->{"quantity"};

    print_rec($rec, $cum_num_shares, $cum_amount, $cum_taxes_owed, $cum_lts_gains, $cum_sts_gains);
    $remaining_losses += $rec->{"totgain"};
}

### Goal #2: sell as many gains as possible, so long as they're offset by losses.
while (int(@recs_losers) > 0 && int(@recs_gainers) > 0) {
    my $cum_gains = $cum_lts_gains + $cum_sts_gains;
    
    my $rec = undef;
    # if we've got too many gains, sell a loss
    if ($cum_gains > (-1 * $TARGET_LOSS)) {
        $rec = shift @recs_losers;
    } else {
        $rec = shift @recs_gainers;
    }

    $cum_amount     += $rec->{"value"}; 
    $cum_lts_gains  += $rec->{"ltgain"}; 
    $cum_sts_gains  += $rec->{"stgain"}; 
    $cum_taxes_owed += $rec->{"tot_tax"};
    $cum_num_shares += $rec->{"quantity"};

    print_rec($rec, $cum_num_shares, $cum_amount, $cum_taxes_owed, $cum_lts_gains, $cum_sts_gains);
}

#my $reached_target_sum = 0;
#while (int(@recs_losers)+int(@recs_gainers)>0) {
#    #foreach my $rec(@sell_order) {
#    if (defined($TARGET_GAINS) && !$reached_target_sum && ($cum_amount > $TARGET_GAINS)) {
#        print("\n^^Reached your target sum.^^\n\n");
#        $reached_target_sum = 1;
#    }

#    my $rec = undef;
#    if (int(@recs_losers) == 0) {
#        $rec = shift @recs_gainers;
#    } elsif (int(@recs_gainers) == 0) {
#        $rec = shift @recs_losers;
#    } elsif ($cum_taxes_owed > 0) {
#        $rec = shift @recs_losers;
#    } elsif ($recs_gainers[0]->{"totgain"} < -1*($cum_lts_gains + $cum_sts_gains)) {
#        $rec = shift @recs_gainers;
#    } else {
#        $rec = shift @recs_losers;
#    }

#    $cum_amount     += $rec->{"value"}; 
#    $cum_lts_gains  += $rec->{"ltgain"}; 
#    $cum_sts_gains  += $rec->{"stgain"}; 
#    $cum_taxes_owed += $rec->{"tot_tax"};
#    $cum_num_shares += $rec->{"quantity"};

#    printf("%32s          %10s %8s %8s             %8s %8s %+0.3f         %12s (%12s)    %12s %12s\n", 
#        $rec->{"fn"},
#        $rec->{"date"}, fmt_num($rec->{"quantity"}), fmt_num($cum_num_shares),
#        fmt_num($rec->{"value"}), fmt_num($rec->{"totgain"}), $rec->{"tpv"},
#        fmt_num($cum_amount),    fmt_num($cum_taxes_owed),
#        fmt_num($cum_lts_gains), fmt_num($cum_sts_gains),
#    );
#}

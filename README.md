# Intelligent Liquidation

Intelligent Liquidation is a script to help you figure out a tax-optimal strategy for selling your investments.  It's just for fun.  Use at your own risk, and check your work!

## Requirements

This script uses the table format specific to Vanguard's Cost Basis screen.  You need to change your cost-basis accounting method to SpecID, both to get the per-lot cost basis as well as to select which lots you want to sell.

## Usage information

```
$ ./il.pl --help

    Usage: ./il.pl [options] <in-file.txt [in-file2.txt [...]]>

Options:
    -h,--help                                   this
    -s,--short-term-tax  SHORT_TAX_RATE         your current short-term capital gains rate (default: 0.28)
    -l,--long-term-tax   LONG_TAX_RATE          your current long-term capital gains rate (default: 0.15)
    -t,--target-sum      TARGET_AMOUNT          minimum amount you wish to liquidate (does not affect calculation)
```

## Example

Copy the table of cost-bases (all rows, all columns) for stock or fund XYZ into `XYZ.txt`.  Repeat for all other securities ABC (`ABC.txt`), ZZZ (`ZZZ.txt`), and so on.  Then:


```
./il.pl XYZ.txt ABC.txt ZZZ.txt
```

